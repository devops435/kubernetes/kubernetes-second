https://docs.google.com/document/d/1L6kSGQBFSfla0mxihLNfIVlV2iKcHQOkAK4mVKShJL0/edit?usp=sharing

# Note
`containerPort` support only default port 80

<br> 

# To run nginx kubernetes, using external service
1. `$ minikube start`
1. `$ kubectl apply -f a-nginx-deployment.yaml`
1. `$ kubectl apply -f c-nginx-external-service.yaml`
1. `$ minikube service external-nginx-service` to access from outside

<br> <br>

# To run nginx kubernetes, using nginx-ingress
1. `$ minikube start`
1. `$ kubectl apply -f a-nginx-deployment.yaml`
1. `$ kubectl apply -f b-nginx-service.yaml`
1. `$ minikube addons enable ingress`
1. `$. kubectl apply -f d-nginx-service.yaml`
1. Map hosts `127.0.0.1 nginxapp.com`
1. `$ minikube tunnel`